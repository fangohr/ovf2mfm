OVF2MFM
=======

What does it do?
----------------

Starting from a 
`OOMMF vector field file OVF 1.0 <http://math.nist.gov/oommf/doc/userguide12a3/userguide/OVF_1.0_format.html>`__ 
(has to be binary with 8 byte per number; presumably only OVF1.0 supported), it computes a gray scale image (i.e. scalar data) as would be seen by a MFM measurement, and saves that in VTK format to a file. The data that is computed is the first derivative of the demagnetisation field. See the file demagderivV2.nb or demagderivV2.pdf for details (depreciated old version: demagderiv.tex). The calculation is based on the multipole expansion used currently in OOMMF.


Public release of code in 2012
------------------------------

November 2012: This program has been written a loooong time ago (~2003), and was never meant to be used by anybody else than the authors. This repository starting with a commit in 2013 does not reflect the full history. The code is neither well written, structured nor documented particularly well. 

However, there have been repeated requests by the OOMMF and micromagnetic community for access to a tool like this. We thus make it available here, but can not offer any support nor ensurances for correctness, stability, functionality etc (see official disclaimer below). Use at your own risk.

We know there is at least one bug in the code. Patches that fix any of these issues are welcome.

Download
--------

The code is available as `Mercurial <http://mercurial.selenic.com>`__ repository on http://bitbucket.org/fangohr/ovf2mfm and a tar ball of the latest development version can be downloaded (`zip <https://bitbucket.org/fangohr/ovf2mfm/get/tip.zip>`__,
`tgz <https://bitbucket.org/fangohr/ovf2mfm/get/tip.tar.gz>`__).


Documentation
-------------

A brief summary and one usage example are available on http://www.southampton.ac.uk/~fangohr/software/ovf2mfm/
The corresponding data files and sources for this are in the ``doc`` subdirectory of the downloaded files.

Richard Boardman, Kristof M. Lebecki and Hans Fangohr



License and Disclaimer
----------------------

This software (ovf2mfm) was developed at the University of Southampton, United
Kingdom. It is released under the 
`GNU General Public License (GPL) <http://www.gnu.org/licenses/gpl.html>`__ as
published by the Free Software Foundation; either version 3, or (at
your option) any later version.

Ovf2mfm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Ovf2mfm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ovf2mfm (in the file license.txt).  
If not, see <http://www.gnu.org/licenses/>.


OVF2MFM is an experimental system. Neither the University of
Southampton nor the authors assume any responsibility whatsoever for
its use by other parties, and makes no guarantees, expressed or
implied, about its quality, reliability, or any other characteristic.

**Copyright:**

2003- Richard Boardman, Hans Fangohr, University of Southampton

2012- Kristof M. Lebecki, University of Konstanz.


