OVF2MFM
=======

Richard Boardman, Kristof Lebecki, Hans Fangohr

Installation
------------

Compile with C compiler. The Makefile has a number of predefined targets. The default target is 'opt' which stands for OPTimisation for the current architecture and compiles the code::
  
  $> make opt
  gcc -ansi -lm -mtune=native -O2 demagderiv.c -o demagderiv
  
and creates the ``demagderiv`` executable.



Usage example
-------------

Given a `OVF1.0 data file <http://math.nist.gov/oommf/doc/userguide12a3/userguide/OVF_1.0_format.html>`__ with a regular grid and double floats (i.e. each double using 8 bytes), the following command can be used::

  $> ./demagderiv <dropletzf350.omf data 100e-9 31 31

This will read the magnetisation data from the ``dropletzf350.omf`` file, and write VTK data to the ``data.vtk`` file (``.vtk`` will be added to the given filename automatically) where the fly height of the MFM tip over the sample surface (in z-direction) is 100e-9 (i.e. 100 nm), and the resolution of the MFM scanning data matrix has 31 by 31 pixels.
You can specify the input file also in any other (valid in UNIX) fashion, e.g.::

  $> cat dropletzf350.omf | ./demagderiv data 100e-9 31 31

Since the binary 8-byte format is required, the OOMMF conversion tool can be used if necessary::

  $> tclsh oommf.tcl avf2ovf -format b8 <dropletzf350.omf | ./demagderiv data 100e-9 31 31

The program should run for a few seconds. We can then visualise the ``data.vtk`` file, for example using the Mayavi2 programme (other options are Paraview, Visit, more recent versions of Paraview)::

  $> mayavi2 -d data.vtk -m Surface

which creates this image (chose ``Module`` of type ``Surface`` and grayscale for the colours):

.. image:: droplet-mfm-signal.png


To provide some context: the magnetisation data in the ``dropletzf350.omf`` OVF file shows a vortex in a geometry of one of the somewhat spherical elements (as was studied in `Boardman et al <http://jap.aip.org/resource/1/japiau/v95/i11/p7037_s1?bypassSSO=1>`__, `postprint <http://eprints.soton.ac.uk/22791/>`__ ) shown here:

.. image:: droplet-geometry.png

and the magnetisation pattern resembles that geometry (we only show a subset of the magnetisation vectors)

.. image:: droplet-vectors.png

For completeness, we mention that we have used `ovf2vtk <http://www.southampton.ac.uk/~fangohr/software/ovf2vtk>`__ conversion tool to create the vtk file that shows the magnetisation vectors above.

Model used
----------

We assume that the MFM tip is magnetised in the z-direction. The
distance to the MFM tip is also measured in z direction from the
sample (that's hard coded; so best to place any thin films in the x-y
plane in the MIF file). The calculation is based on the assumption
that the MFM signal is proportional to the derivative of the
demagnetisation field, so in this case the change of the z component
of demagnetisation field with the coordinate z. The file
`demagderiv.pdf <demagderiv.pdf>`__ describes this in a little more
detail, and also describes how the demagfield derivative is
computed. This is based on the demag field originating from a set of
uniformly magnetised cuboids, and essentially the same technique as
used in OOMMF's demag module. For testing, one can switch to an
approximation where every cube is treated as a magnetic dipole
(ignoring the high order multipole moments from the cuboids).
