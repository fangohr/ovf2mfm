/*
 * demagderiv.c
 *
 * A program to calculate the derivatives of the
 * demagnetising field
 *
 * Richard Boardman, Kristof M. Lebecki, Hans Fangohr
 * Computational Engineering and Design Group
 * (c) 2003 - 2013 University of Southampton
 *     2012 - 2013 University of Konstanz (Germany)
 *
 * The authors nor anybody else assume any responsibility whatsoever
 * for the use of this tool by other parties, and makes no guarantees,
 * expressed or implied, about its quality, reliability, or any other
 * characteristic.
 *
 * Original CVS repository location: 
 * micromagnetics/mm0/tools/demagderiv/demagderiv.c,v 1.5 2003-09-22 07:52:21 rpb01r Exp $
 *
 * August 2008: some tidying up, moving snapshot to latest repository.
 *
 * Nov 2012: Moved sources to http://bitbucket.org/fangohr/ovf2mfm
 * Nov 2012, changes by Kristof Lebecki, Uni Konstanz, nicknamed KL(m):
 * - fixing bug in the cell size along the z-dimension,
 * - precising the meaning of the parameter "fly height of the MFM tip",
 * - higher-expansion of the dipolar interactions (like in the OOMMF'2007),
 * - making sure that first order derivative of the effective field is used,
 * - omf input file is now stdin (not a command line parameter anymore).
 *
 * License:
 * 
 * This file is part of ovf2mfm.
 * 
 * Ovf2mfm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Ovf2mfm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with ovf2mfm (in the file license.txt).  
 * If not, see <http://www.gnu.org/licenses/>.
 * 
 *
 */

/* Gather header files (note: build with gcc -lm) */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* create a struct for vectors */

typedef struct vector
{
  double x;
  double y;
  double z;
} Vector;

/* KL(m). to save space and computation time */
typedef struct dummyVector
{
  double z;
} dummyVector;

double magnitude(Vector* a)
{
  /* speed up of approximately 400% by not using pow (fangohr 17/09/2003 18:33)
     return sqrt( pow(a->x, 2.) + pow(a->y, 2.) + pow(a->z, 2.)); */
  return sqrt( a->x*a->x + a->y*a->y + a->z*a->z);
}

double dotproduct(Vector* a, Vector* b)
{
  return (a->x * b->x) + (a->y * b->y) + (a->z * b->z);
}

/* a 'one-size-fits-all' fatal error exit procedure; to be tidy we should free our mallocs (TODO) */

void lazy_error(char *message)
{
  fprintf(stderr, "Error: %s\n", message);
  exit(3); /* "lazy_errored" */
}

/* a procedure to feed back usage information */

void usage(char *command)
{
  printf("Usage: %s [output] [tip height] [target res x] [target res y] [dipol (method)]\n", command);
  printf("\nwhere:\n");
  printf("  output is some filename (without .vtk)\n");
  printf("  tip height is the fly height of the MFM tip (m)\n");
  printf("  target res x is the number of cells in x direction\n");
  printf("  target res y is the number of cells in y direction\n");
  printf("  dipole is an optional specification of the faster but less acurate method (optional)\n");
  printf("Input data (OOMMF magnetisation file) is read from the standard input.\n\n");  
}

/* program information */

void info()
{
  printf("demagderiv - a program to calculate demagnetising fields\n");
  printf("             as would be seen by a magnetic force microscope\n");
}

void author()
{
  printf("Richard Boardman, Kristof M. Lebecki, Hans Fangohr\n");
  printf("Computational Engineering and Design Group\n");
  printf("(c) 2003- University of Southampton\n\n");
  printf("(c) 2012- University of Konstanz\n\n");
}

/* To inline (some functions) or not to inline? */
/* Inline gives you ca. 17% speed-up, you can get, however, 
   cross-platform compilation issues. */
/* The choice is yours ;) KL(m) */

/************* MFM SIGNAL FUNCTIONS, written by Kristof Lebecki, 2012 *************/

/*inline*/ double pow2(const double x) {
  return x*x;
}
/*inline*/ double pow3(const double x) {
  return x*x*x;
}
/*inline*/ double pow4(const double x) {
  double tmp = x*x;
  return tmp*tmp;
}
/*inline*/ double pow6(const double x) {
  double tmp = x*x*x;
  return tmp*tmp;
}

/* We assume dx,dy,dz to be always constant */
struct fun_args_dx {
  double dx,  dy,  dz;
  double dx2, dy2, dz2;
  double dx4, dy4, dz4;
};

/* Powers of z and y will sometimes change inside the loop */
struct fun_args_x {
  double x, y, z;
  double    y2, z2;
  double    y4, z4;
  double    y6, z6;
};

/* We have formulas for three different cases */
/* Both cubic and noncubic are high-order */
/* Cubic case is only a special (and faster) case, where the cells are cubes */
enum Tequation_type {dipole, cubic, noncubic};
/* Evaluation time of these procedures */
/* 1.00 : 1.15 : 1.72 */

/* Common prefactor for the dipolar form */
/* We could try to reformulate pow(*,7.5) into sqrt(pow15(*)), but lets leave it as it is. */
/*inline*/ double dnzFactorDipol(const struct fun_args_dx e, const struct fun_args_x f) {
  return (64*3*e.dx*e.dy*e.dz)/(4*pow(pow2(f.x) + f.y2 + f.z2, 3.5));
}
/* Dipolar form */
/* CForm[simplified[[1]]] */
/*inline*/ double dnXZzDipol(const struct fun_args_x f) {
  return -(f.x*(pow2(f.x) + f.y2 - 4*f.z2));
}
/* CForm[simplified[[2]]] */
/*inline*/ double dnYZzDipol(const struct fun_args_x f) {
  return -(f.y*(pow2(f.x) + f.y2 - 4*f.z2));
}
/* CForm[simplified[[3]]] */
/*inline*/ double dnZZzDipol(const struct fun_args_x f) {
  return -3*(pow2(f.x) + f.y2)*f.z + 2*f.z*f.z2;
}

/* Common forefactor */
/* We could try to reformulate pow(*,7.5) into sqrt(pow15(*)), but lets leave it as it is. */
/*inline*/ double dnzFactor(const struct fun_args_dx e, const struct fun_args_x f) {
  return (e.dx*e.dy*e.dz)/pow(pow2(f.x) + f.y2 + f.z2, 7.5);
}

/* Cubic form */
/* CForm[simplified[[1]]] */
/*inline*/ double dnXZzCubic(const struct fun_args_dx e, const struct fun_args_x f) {
	/* Here, everywhere only dx4 */
  return f.x*(-48*(pow2(f.x) + f.y2 - 4*f.z2)*
      pow4(pow2(f.x) + f.y2 + f.z2) + 
     7*e.dx4*(7*pow6(f.x) - 6*pow4(f.x)*f.y2 + 13*f.y6 - 
        3*(47*pow4(f.x) - 20*pow2(f.x)*f.y2 + 65*f.y4)*f.z2 + 
        15*(15*pow2(f.x) + 11*f.y2)*f.z4 - 56*f.z6));
}
/* CForm[simplified[[2]]] */
/*inline*/ double dnYZzCubic(const struct fun_args_dx e, const struct fun_args_x f) {
	/* Here, everywhere only dx4 */
  return f.y*(-48*(pow2(f.x) + f.y2 - 4*f.z2)*
      pow4(pow2(f.x) + f.y2 + f.z2) + 
     7*e.dx4*(13*pow6(f.x) - 6*pow2(f.x)*f.y4 + 7*f.y6 - 
        3*(65*pow4(f.x) - 20*pow2(f.x)*f.y2 + 47*f.y4)*f.z2 + 
        15*(11*pow2(f.x) + 15*f.y2)*f.z4 - 56*f.z6));
}
/* CForm[simplified[[3]]] */
/*inline*/ double dnZZzCubic(const struct fun_args_dx e, const struct fun_args_x f) {
	/* Here, everywhere only dx4 */
  return -f.z*(48*(3*(pow2(f.x) + f.y2) - 2*f.z2)*
        pow4(pow2(f.x) + f.y2 + f.z2) - 
       7*e.dx4*(43*pow6(f.x) + 30*pow4(f.x)*f.y2 + 
          30*pow2(f.x)*f.y4 + 43*f.y6 - 
          15*(15*pow4(f.x) + 8*pow2(f.x)*f.y2 + 15*f.y4)*f.z2 + 
          147*(pow2(f.x) + f.y2)*f.z4 - 14*f.z6));
}

/* Non-cubic form */
/* CForm[simplified[[1]]] */
/*inline*/ double dnXZzNonCubic(const struct fun_args_dx e, const struct fun_args_x f) {
  return f.x*(-14*e.dy4*(pow6(f.x) - 15*pow4(f.x)*f.y2 + 16*f.y6 - 
        6*(pow4(f.x) - 25*pow2(f.x)*f.y2 + 40*f.y4)*f.z2 - 
        15*(pow2(f.x) - 11*f.y2)*f.z4 - 8*f.z6) - 
     14*e.dx4*(8*pow6(f.x) - 15*pow2(f.x)*(f.y2 - 15*f.z2)*
         (f.y2 + f.z2) + 
        5*(f.y2 - 8*f.z2)*pow2(f.y2 + f.z2) - 
        12*pow4(f.x)*(f.y2 + 13*f.z2)) + 
     2*(-24*(pow2(f.x) + f.y2 - 4*f.z2)*
         pow4(pow2(f.x) + f.y2 + f.z2) + 
        30*e.dz2*pow2(pow2(f.x) + f.y2 + f.z2)*
         (pow2(pow2(f.x) + f.y2) - 12*(pow2(f.x) + f.y2)*f.z2 + 
           8*f.z4) + 7*e.dz4*
         (-5*pow3(pow2(f.x) + f.y2) + 
           120*pow2(pow2(f.x) + f.y2)*f.z2 - 
           240*(pow2(f.x) + f.y2)*f.z4 + 64*f.z6)) + 
     5*e.dy2*(7*e.dz2*(pow2(pow2(f.x) + f.y2)*
            (-pow2(f.x) + 8*f.y2) + 
           15*(pow2(f.x) - 11*f.y2)*(pow2(f.x) + f.y2)*f.z2 + 
           240*f.y2*f.z4 - 16*f.z6) + 
        4*pow2(pow2(f.x) + f.y2 + f.z2)*
         (pow4(f.x) - 6*f.y4 + 51*f.y2*f.z2 - 6*f.z4 - 
           5*pow2(f.x)*(f.y2 + f.z2))) + 
     5*e.dx2*(21*e.dz2*(2*pow6(f.x) + 3*pow4(f.x)*f.y2 - f.y6 + 
           15*(-3*pow4(f.x) - 2*pow2(f.x)*f.y2 + f.y4)*f.z2 + 
           80*pow2(f.x)*f.z4 - 16*f.z6) - 
        4*pow2(pow2(f.x) + f.y2 + f.z2)*
         (4*pow4(f.x) + pow2(f.x)*(f.y2 - 41*f.z2) - 
           3*(f.y2 - 6*f.z2)*(f.y2 + f.z2)) + 
        7*e.dy2*(2*pow6(f.x) - 21*pow4(f.x)*(f.y2 + f.z2) - 
           15*pow2(f.x)*(f.y4 - 20*f.y2*f.z2 + f.z4) + 
           (f.y2 + f.z2)*
            (8*f.y4 - 83*f.y2*f.z2 + 8*f.z4))));
}
/* CForm[simplified[[2]]] */
/*inline*/ double dnYZzNonCubic(const struct fun_args_dx e, const struct fun_args_x f) {
  return f.y*(-14*e.dx4*(16*pow6(f.x) - 240*pow4(f.x)*f.z2 - 
        15*pow2(f.x)*(f.y2 - 11*f.z2)*(f.y2 + f.z2) + 
        (f.y2 - 8*f.z2)*pow2(f.y2 + f.z2)) - 
     14*e.dy4*(5*pow6(f.x) + 8*f.y6 - 156*f.y4*f.z2 + 
        225*f.y2*f.z4 - 40*f.z6 - 
        15*pow4(f.x)*(f.y2 + 2*f.z2) - 
        3*pow2(f.x)*(4*f.y4 - 70*f.y2*f.z2 + 25*f.z4)) + 
     2*(-24*(pow2(f.x) + f.y2 - 4*f.z2)*
         pow4(pow2(f.x) + f.y2 + f.z2) + 
        30*e.dz2*pow2(pow2(f.x) + f.y2 + f.z2)*
         (pow2(pow2(f.x) + f.y2) - 12*(pow2(f.x) + f.y2)*f.z2 + 
           8*f.z4) + 7*e.dz4*
         (-5*pow3(pow2(f.x) + f.y2) + 
           120*pow2(pow2(f.x) + f.y2)*f.z2 - 
           240*(pow2(f.x) + f.y2)*f.z4 + 64*f.z6)) + 
     5*e.dy2*(-21*e.dz2*(pow6(f.x) - 3*pow2(f.x)*f.y4 - 
           2*f.y6 - 15*(pow2(f.x) - 3*f.y2)*(pow2(f.x) + f.y2)*
            f.z2 - 80*f.y2*f.z4 + 16*f.z6) + 
        4*pow2(pow2(f.x) + f.y2 + f.z2)*
         (3*pow4(f.x) - 4*f.y4 + 41*f.y2*f.z2 - 18*f.z4 - 
           pow2(f.x)*(f.y2 + 15*f.z2))) + 
     5*e.dx2*(7*e.dz2*((8*pow2(f.x) - f.y2)*
            pow2(pow2(f.x) + f.y2) + 
           15*(-11*pow4(f.x) - 10*pow2(f.x)*f.y2 + f.y4)*f.z2 + 
           240*pow2(f.x)*f.z4 - 16*f.z6) - 
        4*pow2(pow2(f.x) + f.y2 + f.z2)*
         (6*pow4(f.x) - f.y4 + 5*f.y2*f.z2 + 6*f.z4 + 
           pow2(f.x)*(5*f.y2 - 51*f.z2)) + 
        7*e.dy2*(8*pow6(f.x) - 15*pow4(f.x)*(f.y2 + 5*f.z2) + 
           (f.y2 + f.z2)*
            (2*f.y4 - 23*f.y2*f.z2 + 8*f.z4) - 
           3*pow2(f.x)*(7*f.y4 - 100*f.y2*f.z2 + 25*f.z4))));
}
/* CForm[simplified[[3]]] */
/*inline*/ double dnZZzNonCubic(const struct fun_args_dx e, const struct fun_args_x f) {
  return f.z*(-42*e.dy4*(pow6(f.x) - 15*pow4(f.x)*f.y2 + 16*f.y6 + 
        10*f.y2*(3*pow2(f.x) - 8*f.y2)*f.z2 - 
        3*(pow2(f.x) - 15*f.y2)*f.z4 - 2*f.z6) - 
     42*e.dx4*(16*pow6(f.x) + f.y6 - 80*pow4(f.x)*f.z2 - 
        3*f.y2*f.z4 - 2*f.z6 - 
        15*pow2(f.x)*(f.y2 - 3*f.z2)*(f.y2 + f.z2)) - 
     2*(24*(3*(pow2(f.x) + f.y2) - 2*f.z2)*
         pow4(pow2(f.x) + f.y2 + f.z2) - 
        10*e.dz2*pow2(pow2(f.x) + f.y2 + f.z2)*
         (15*pow2(pow2(f.x) + f.y2) - 40*(pow2(f.x) + f.y2)*f.z2 + 
           8*f.z4) + 7*e.dz4*
         (35*pow3(pow2(f.x) + f.y2) - 
           210*pow2(pow2(f.x) + f.y2)*f.z2 + 
           168*(pow2(f.x) + f.y2)*f.z4 - 16*f.z6)) - 
     5*e.dy2*(-4*pow2(pow2(f.x) + f.y2 + f.z2)*
         (3*(pow2(f.x) - 6*f.y2)*(pow2(f.x) + f.y2) - 
           (pow2(f.x) - 41*f.y2)*f.z2 - 4*f.z4) + 
        7*e.dz2*(5*(pow2(f.x) - 8*f.y2)*pow2(pow2(f.x) + f.y2) - 
           15*(pow2(f.x) - 15*f.y2)*(pow2(f.x) + f.y2)*f.z2 - 
           12*(pow2(f.x) + 13*f.y2)*f.z4 + 8*f.z6)) + 
     5*e.dx2*(7*e.dz2*(5*(8*pow2(f.x) - f.y2)*
            pow2(pow2(f.x) + f.y2) + 
           15*(-15*pow4(f.x) - 14*pow2(f.x)*f.y2 + f.y4)*f.z2 + 
           12*(13*pow2(f.x) + f.y2)*f.z4 - 8*f.z6) + 
        7*e.dy2*(8*pow6(f.x) - 75*pow4(f.x)*f.y2 - 
           75*pow2(f.x)*f.y4 + 8*f.y6 - 
           15*(pow4(f.x) - 20*pow2(f.x)*f.y2 + f.y4)*f.z2 - 
           21*(pow2(f.x) + f.y2)*f.z4 + 2*f.z6) - 
        4*pow2(pow2(f.x) + f.y2 + f.z2)*
         (18*pow4(f.x) - 3*f.y4 + f.y2*f.z2 + 4*f.z4 + 
           pow2(f.x)*(15*f.y2 - 41*f.z2))));
}

/************* MFM SIGNAL FUNCTIONS, end *************/

int main(int argc, char *argv[])
{
  unsigned int target_x, target_y;

  /* pretty */
  char *blue, *nocol, *yellow, *green, *red;
  blue   = "\e[1;34m";
  nocol  = "\e[0m";
  yellow = "\e[1;33m";
  green  = "\e[1;32m";
  red    = "\e[1;31m";

  /* initialise our variables for the command line parse */

  char *outputfile;
  char *tipdistchar;
  char *targetrescharx;
  char *targetreschary;
  char *dipolMethod;

  /* initialise variables we will derive from the input data */

  Vector cellsize;
  int numspins;

  /* initialise variables we will need to read the data */

  FILE   *infile = stdin;
  size_t infilesize;
  int    intchr;
  size_t dataptr;

  FILE   *outfile;
  
  char   *bfstartstring;
  char   *bfendstring;
  int    bfstartstringsize, bfendstringsize;
  unsigned int    i, j; /* counters */
  int    foundstring, foundposition;
  int    bfstart, bfend, bfsize;
  int    blocksize;

  /* variables linked to second derivative demag field computation */

  Vector tip;
  unsigned int tx, ty;
  int arraypos;
  unsigned int tippos;

  unsigned char tempchar;  
  double*       flatspins;

  char *xdimid       = "# xnodes: ";
  char *ydimid       = "# ynodes: ";
  char *zdimid       = "# znodes: ";

  char *xcellid      = "# xstepsize: ";
  char *ycellid      = "# ystepsize: ";
  char *zcellid      = "# zstepsize: ";

  int xdimidsize     = strlen(xdimid);
  int ydimidsize     = strlen(ydimid);
  int zdimidsize     = strlen(zdimid);

  int xcellidsize    = strlen(xcellid);
  int ycellidsize    = strlen(ycellid);
  int zcellidsize    = strlen(zcellid);

  int xdimidpos;
  int ydimidpos;
  int zdimidpos;

  int xcellidpos;
  int ycellidpos;
  int zcellidpos;

  int finalposition;

  /* variables for helping with normalisation later */

  Vector max_cd;
  Vector min_cd;
  
  max_cd.x = 0;
  max_cd.y = 0;
  max_cd.z = 0;
  min_cd.x = 0;
  min_cd.y = 0;
  min_cd.z = 0;
  
  /* KL(m). Test the MFM functions */
  /*
  const struct fun_args_dx tmp1 = { 
     3
    ,2
    ,1
    ,pow2(3)
    ,pow2(2)
    ,pow2(1)
    ,pow4(3)
    ,pow4(2)
    ,pow4(1) 
  };
  const struct fun_args_x tmp2 = { 
     4
    ,3
    ,2
    ,pow2(3)
    ,pow2(2)
    ,pow4(3)
    ,pow4(2) 
    ,pow6(3)
    ,pow6(2)
  };
  printf("Testing\n");
  printf("%g %g %g %g %g %g\t%g %g %g\n\t\t\t\t\t\t\t\t", 
    tmp2.x, tmp2.y, tmp2.z, tmp1.dx, tmp1.dy, tmp1.dz, 
    dnzFactorDipol(tmp1,tmp2)*dnXZzDipol(tmp2),    dnzFactorDipol(tmp1,tmp2)*dnYZzDipol(tmp2),    dnzFactorDipol(tmp1,tmp2)*dnZZzDipol(tmp2));
  printf("%g %g %g\n\t\t\t\t\t\t\t\t", 
    dnzFactor(tmp1,tmp2)*dnXZzCubic(tmp1,tmp2),    dnzFactor(tmp1,tmp2)*dnYZzCubic(tmp1,tmp2),    dnzFactor(tmp1,tmp2)*dnZZzCubic(tmp1,tmp2));
  printf("%g %g %g\n", 
    dnzFactor(tmp1,tmp2)*dnXZzNonCubic(tmp1,tmp2), dnzFactor(tmp1,tmp2)*dnYZzNonCubic(tmp1,tmp2), dnzFactor(tmp1,tmp2)*dnZZzNonCubic(tmp1,tmp2));
  abort();
  x y z dx dy dz: prog-dip / (prog-cube) / prog-noncube / analythical
  0 0 20 1 1 1	0 0 0.0006
								0 0 0.000599996
								0 0 0.000599996
                0 0 0.000599996
  1 2 20 1 1 1	5.72676e-05 0.000114535 0.0005637
								5.7267e-05 0.000114534 0.000563697
								5.7267e-05 0.000114534 0.000563697
                0.0000572669778 0.000114533967 0.000563697320
  1 2 200 1 1 1	5.99719e-10 1.19944e-09 5.99625e-08
								5.99719e-10 1.19944e-09 5.99625e-08
								5.99719e-10 1.19944e-09 5.99625e-08
                5.99718831*10^-10, 1.19943766*10^-9, 5.99625123*10^-8
  1 2 3 1 1 1	  0.144929 0.289858 0.0420761
								0.145008 0.290777 0.0428204
								0.145008 0.290777 0.0428204
                0.144977741, 0.290787940, 0.0428488726
  0 0 20 1 2 3	0 0 0.0036
								0 0 0.0036992
                0., -1.89478*10^-14, 0.00369921
  1 2 20 1 2 3	0.000343606 0.000687212 0.0033822
								0.000359085    0.000711297    0.00346985                
                0.000359093432 0.000711292579 0.00346986543
  1 2 200 1 2 3	3.59831e-09 7.19663e-09 3.59775e-07
								3.59994e-09      7.19921e-09      3.59872e-07
                3.59994340*10^-9 7.19921164*10^-9 3.59872472*10^-7
  11 12 13 3 2 1	  0.000764562 0.000834068 -0.0010047
                    0.0007721      0.000857018    -0.000991195
                    0.000772096153 0.000857013485 -0.000991196789
  111 112 113 3 2 1	8.10341e-08 8.17641e-08 -1.54398e-07
                    8.10341e-08 8.17641e-08 -1.54398e-07
                    7.26262371*10^-8, 7.98368584*10^-8, -9.23669403*10^-8 <- Newells formulas fail numerically for large distancies
  4 3 2 3 2 1	  -0.0789409 -0.0592057 -0.293836
								-0.0266845    -0.0146723     -0.379502 <- for such small distancies higher-order expansion is needed
                -0.0228352822 -0.00816931169 -0.375508957
  */

  /* send some application info to the command line */

  printf("%s", green);
  info();
  printf("%s", nocol);
  author();
  
  /*
   * What do we want from the command line?
   *
   * 1 - an input file  (in OOMMF format)
   * 2 - an output file (in some format)
   * 3 - an MFM tip fly-height (some double in metres)
   *
   */

  /* first, check for the right number of arguments */

  if (argc != 5 && argc != 6)
    {
      usage(argv[0]);
      lazy_error("Check command line arguments and try again :)");
    }
  
  /* assign variables */

  outputfile     = argv[1];
  tipdistchar    = argv[2];
  targetrescharx = argv[3];
  targetreschary = argv[4];
  if(6==argc) {
    dipolMethod = argv[5];
    if(0 != strcmp("dipole", dipolMethod))
      lazy_error("Error! Last oommand-line parameter should be: dipole\n");
  } else
  dipolMethod = "";

  printf("Will attempt to write results to file \"%s%s%s\"\n", blue, outputfile, nocol);

  /* try to parse the tip height and target res*/

  const double tipdistance = atof(tipdistchar);
  target_x    = atoi(targetrescharx);
  target_y    = atoi(targetreschary);

  /* try to read the file : assume is binary8 for the moment */

  printf("Trying to read standard input ...\n");
  fflush(stdout);

  printf("Seeking\n");

  if (fseek(infile, 0, SEEK_END))
    {
      fclose(infile);
      lazy_error("Something went odd whilst flicking through the input file");
    } 

  /* input file is good to go, so let's get the filesize and rewind it */

  infilesize = ftell(infile);
  rewind(infile);

  /* create a uchar array of size infilesize */
  printf("Creating uchar array of size %d\n", (int) infilesize);

  /* replaced array declaration with malloced pointer to avoid blowing heap */
  unsigned char* infiledata = malloc(sizeof(unsigned char) * infilesize);
  printf("Setting dataptr to 0\n");
  dataptr = 0;

  /* read in the data to the uchar array */

  while ((intchr = fgetc(infile)) != EOF)
    {
      infiledata[dataptr++] = (unsigned char) intchr;
      if (dataptr % 1000 == 0) {
	printf("\rRead %s%d%s bytes of %s%d%s      ", yellow, (int) dataptr, nocol, green, (int) infilesize, nocol);
	fflush(stdout);
      }
    }
  printf("\rRead %s%d%s bytes of %s%d%s      ", yellow, (int) dataptr, nocol, green, (int) infilesize, nocol);
  printf("\n");

  /* set the search string */

  bfstartstring     = "# Begin: Data Binary 8\n";
  bfstartstringsize = strlen(bfstartstring);

  foundstring   = 0; /* initialise the flag */
  foundposition = 0; /* initialise the position */

  /* hunt for the start of the data */

  for (i = 0; i < (infilesize - bfstartstringsize); i++)
    {
      if (infiledata[i] == bfstartstring[0])
	{
	  for (j = 0; j < bfstartstringsize; j++)
	    {
	      if (infiledata[i+j] == bfstartstring[j])
		{
		  foundstring = 1;
		}
	      else
		{
		  /* it doesn't match */
		  foundstring = 0;
		  break;
		}	      
	    }
	  if (foundstring == 1)
	    {
	      foundposition = i;
	      break;
	    }
	}
    }

  /* have we found the string? */
    if (foundstring == 0)
      {
        lazy_error("Don't understand... we only do binary 8 here, sorry");
      }
  
    bfstart = foundposition + bfstartstringsize;
    
    /* hunt for the end of the data */
  
    bfendstring     = "# End: Data Binary 8\n";
    bfendstringsize = strlen(bfendstring);
  
    foundstring     = 0;
    foundposition   = 0;
  
    for (i = 0; i < (infilesize - bfendstringsize); i++)
      {
        if (infiledata[i] == bfendstring[0])
        {
          for (j = 0; j < bfendstringsize; j++)
            {
              if (infiledata[i+j] == bfendstring[j])
              {
                  foundstring = 1;
               }
               else
                {
                 /* no match now... oh well */
                  foundstring = 0;
                  break;
                }
          }
          if (foundstring == 1)
            {
              foundposition = i;
              break;
            }
        }
      }
  
    if (foundstring == 0)
      {
        lazy_error("Found beginning of data but it seems to go on forever...\n");
      }
  
    bfend = foundposition;
    bfsize = bfend - bfstart;
  
    printf("Data goes from byte %d to byte %d (size %d bytes)\n", bfstart, bfend, bfsize);
  
    /* now define a friendlier array ... */
 
    unsigned char* binarydata = malloc(sizeof(unsigned char) * bfsize);
 
    /* ... and make it */
  
    printf("Reconstructing data array... ");
    fflush(stdout);
  
    for (i = bfstart; i < bfend; i++)
      {
        binarydata[i - bfstart] = infiledata[i];
      }
    
    printf("%sdone%s\n", green, nocol);
  
    /* deal with the fact x86 machines are little endian ... */
   
    printf("Little endian reordering... ");
    fflush(stdout);
  
    blocksize = 8;
  
    for (i = 0; i < bfsize; i+=blocksize)
      {
        for (j = 0; j < (blocksize / 2); j++)
        {
          tempchar = binarydata[i+j];
          binarydata[i+j] = binarydata[i+(blocksize-j-1)];
          binarydata[i+(blocksize-j-1)] = tempchar;
        }
      }
  
    printf("%sdone%s\n", green, nocol);
  
    /* convert to double array */
  
    flatspins = (double *) binarydata;
  
    /* just check everything's good */
  
    if (flatspins[0] != 1.23456789012345e14)
      {
        lazy_error("Oh dear. This is a little endian machine? Sorry :(");
      }
  
    printf("Ignoring the first data slice and computing number of stored spins\n");
  
    numspins = (bfsize - 1) / (3 * blocksize); /* -1 for 1.234..., three vectors per block */
    Vector* spins = malloc(sizeof(Vector) * numspins);
  
    printf("Building spin vector over %d spins... ", numspins);

  for (i = 0; i < numspins; i++) /* offset for the moment by 3 rather than 1 (!) */
    {
      spins[i].x = flatspins[1+(i*3)];
      spins[i].y = flatspins[2+(i*3)];
      spins[i].z = flatspins[3+(i*3)];
    }

  /* spin verification could go here... */

  printf("%sdone%s\n", green, nocol);

  /* gather x cell size */

  foundstring   = 0;
  foundposition = 0;
  finalposition = 0;

  for (i = 0; i < (infilesize - xcellidsize); i++)
    {
      if (infiledata[i] == xcellid[0])
	{
	  for (j = 0; j < xcellidsize; j++)
	    {
	      if (infiledata[i+j] == xcellid[j]) foundstring = 1;
	      else
		{
		  /* doesn't match */
		  foundstring = 0;
		  break;
		}
	    }
	  if (foundstring == 1)
	    {
	      foundposition = i;
	      i+=xcellidsize;
	      while(1)
		{	
		  if (infiledata[i] == '\n') break;
		  finalposition = i++;
		}
	      break;
	    }
	}
    }

  xcellidpos = foundposition + xcellidsize;
  char xcellidvalue[finalposition - xcellidpos];
  for (i = 0; i < (1+(finalposition - xcellidpos)); i++) xcellidvalue[i] = infiledata[xcellidpos + i];

 /* gather y cell size */

  foundstring   = 0;
  foundposition = 0;
  finalposition = 0;

  for (i = 0; i < (infilesize - ycellidsize); i++)
    {
      if (infiledata[i] == ycellid[0])
	{
	  for (j = 0; j < ycellidsize; j++)
	    {
	      if (infiledata[i+j] == ycellid[j]) foundstring = 1;
	      else
		{
		  /* doesn't match */
		  foundstring = 0;
		  break;
		}
	    }
	  if (foundstring == 1)
	    {
	      foundposition = i;
	      i+=ycellidsize;
	      while(1)
		{	
		  if (infiledata[i] == '\n') break;
		  finalposition = i++;
		}
	      break;
	    }
	}
    }

  ycellidpos = foundposition + ycellidsize;
  char ycellidvalue[finalposition - ycellidpos];
  for (i = 0; i < (1+(finalposition - ycellidpos)); i++) ycellidvalue[i] = infiledata[ycellidpos + i];

 /* gather z cell size */

  foundstring   = 0;
  foundposition = 0;
  finalposition = 0;

  for (i = 0; i < (infilesize - zcellidsize); i++)
    {
      if (infiledata[i] == zcellid[0])
	{
	  for (j = 0; j < zcellidsize; j++)
	    {
	      if (infiledata[i+j] == zcellid[j]) foundstring = 1;
	      else
		{
		  /* doesn't match */
		  foundstring = 0;
		  break;
		}
	    }
	  if (foundstring == 1)
	    {
	      foundposition = i;
	      i+=zcellidsize;
	      while(1)
		{	
		  if (infiledata[i] == '\n') break;
		  finalposition = i++;
		}
	      break;
	    }
	}
    }

  zcellidpos = foundposition + zcellidsize;
  char zcellidvalue[finalposition - zcellidpos+1];
  for (i = 0; i < (1+(finalposition - zcellidpos)); i++) zcellidvalue[i] = infiledata[zcellidpos + i];
  /* KL(m) */
  /* No idea, what following line was intented to do. */
  /* IMHO it is better to comment it out. */
  /* However, very similar code around line 691 still makes sense... */
  /* zcellidvalue[finalposition - zcellidpos] = 0; */

  /* pop cell size into vector */

  cellsize.x = atof(xcellidvalue);
  cellsize.y = atof(ycellidvalue);
  cellsize.z = atof(zcellidvalue);

  /* get x dimension */

  foundstring   = 0;
  foundposition = 0;
  finalposition = 0;

  for (i = 0; i < (infilesize - xdimidsize); i++)
    {
      if (infiledata[i] == xdimid[0])
	{
	  for (j = 0; j < xdimidsize; j++)
	    {
	      if (infiledata[i+j] == xdimid[j]) foundstring = 1;
	      else
		{
		  /* no match */
		  foundstring = 0;
		  break;
		}	     
	    }
	  if (foundstring == 1)
	    {
	      foundposition = i;
	      i+=xdimidsize;
	      while(1)
		{
		  if (infiledata[i] == '\n') break;
		  finalposition = i++;
		}
	      break;
	    }
	}
    }

  xdimidpos = foundposition + xdimidsize;
 
  /* note: in the char below we need the '+1' hack to fix
   * the value we're after. More a 'note-to-self'... */

  char xdimidvalue[(finalposition - xdimidpos)+1];
  for (i = 0; i < (1+(finalposition - xdimidpos)); i++) xdimidvalue[i] = infiledata[xdimidpos + i];

  /* get y dimension */

  foundstring   = 0;
  foundposition = 0;
  finalposition = 0;

  for (i = 0; i < (infilesize - ydimidsize); i++)
    {
      if (infiledata[i] == ydimid[0])
	{
	  for (j = 0; j < ydimidsize; j++)
	    {
	      if (infiledata[i+j] == ydimid[j]) foundstring = 1;
	      else
		{
		  /* no match */
		  foundstring = 0;
		  break;
		}	     
	    }
	  if (foundstring == 1)
	    {
	      foundposition = i;
	      i+=ydimidsize;
	      while(1)
		{
		  if (infiledata[i] == '\n') break;
		  finalposition = i++;
		}
	      break;
	    }
	}
    }

  ydimidpos = foundposition + ydimidsize;
  char ydimidvalue[(finalposition - ydimidpos)+1];
  for (i = 0; i < (1+(finalposition - ydimidpos)); i++) ydimidvalue[i] = infiledata[ydimidpos + i];

  /* get z dimension */

  foundstring   = 0;
  foundposition = 0;
  finalposition = 0;

  for (i = 0; i < (infilesize - zdimidsize); i++)
    {
      if (infiledata[i] == zdimid[0])
	{
	  for (j = 0; j < zdimidsize; j++)
	    {
	      if (infiledata[i+j] == zdimid[j]) foundstring = 1;
	      else
		{
		  /* no match */
		  foundstring = 0;
		  break;
		}	     
	    }
	  if (foundstring == 1)
	    {
	      foundposition = i;
	      i+=zdimidsize;
	      while(1)
		{
		  if (infiledata[i] == '\n') break;
		  finalposition = i++;
		}
	      break;
	    }
	}
    }

  zdimidpos = foundposition + zdimidsize;
  char zdimidvalue[finalposition - zdimidpos+1+1];
  for (i = 0; i < (1+(finalposition - zdimidpos)); i++) zdimidvalue[i] = infiledata[zdimidpos + i];
  zdimidvalue[finalposition - zdimidpos+1] = '\0';
    
  const unsigned int xdim = atoi(xdimidvalue);
  const unsigned int ydim = atoi(ydimidvalue);
  const unsigned int zdim = atoi(zdimidvalue);

  /* KL(m) */
  /* Lets make sure that tip distance is positive (and large). */
  if(tipdistance < cellsize.z/10) {
    char buffer [999];
    sprintf(buffer,
      "Error! Tip distance (%g) should not be low as compared to the z-cellsize (%g)\n",
      tipdistance, cellsize.z);
    lazy_error(buffer);
  }
  /* Here we decide, what equations will be used for the demagnetization field computation */
  enum Tequation_type equation_type;
  if(fabs(cellsize.x-cellsize.y)<cellsize.x/1000 && fabs(cellsize.x-cellsize.z)<cellsize.x/1000) {
    equation_type= cubic;
  } else {
    equation_type= noncubic;
  }
  if(0 == strcmp(dipolMethod, "dipole"))
    equation_type= dipole; /* e.g: for debugging */
  printf("Using MFM formulas of type: ");
  switch (equation_type) {
    case dipole: 
      printf("dipolar.\n");
      break;
    case cubic:  
      printf("higher-order cubic.\n");
      break;
    case noncubic: 
      printf("higher-order non-cubic.\n");
      break;
  }
  
  /* One can now evaluate MFM tip position/height/distance */  
  const double tipposition = tipdistance + ((double)zdim-0.5)*cellsize.z;
  printf("Working with an MFM tip position %s%g%s, "
    "thus the distance to the sample is %s%g%s (m).\n", 
    yellow, tipposition, nocol, yellow, tipdistance, nocol);

  printf("Cell size is %g x %g x %g metres\n", cellsize.x, cellsize.y, cellsize.z);
  printf("Working on geometry %d x %d x %d = %d cells\n", xdim, ydim, zdim, xdim*ydim*zdim);

  /* close input file */

  fclose(infile);

  /*
   *
   * now we have finished parsing the input file, we can do something useful 
   *
   */

  /* we need three arrays to calculate the second derivative 
   * through central differences; remember the tip lives in
   * two variable (x, y) dimensions with a fixed z component */

  printf("Allocating three vector arrays of size %d * %d * %d = %s%d%s bytes... ", xdim, ydim, 
      (int) sizeof(dummyVector), red, xdim*ydim*((int) sizeof(dummyVector)), nocol);

  dummyVector *central_point_field = (dummyVector *) malloc (target_x*target_y*sizeof(dummyVector));

  printf("%sdone%s\n", green, nocol);

  printf("[ in %s%g%s iterations ]\n", yellow, (double)(ydim*xdim*zdim*ydim*xdim), nocol);
  printf("Now a little text graphic to show our tip position whilst computing:\n");

  /* first iteration; calculate the central layer around tipposition */

  /* set the tip z position */

  tip.z = tipposition;

  /* reset any rogue values (shouldn't need to here) */

  tippos   = 0;
  arraypos = 0;

  Vector m;

  Vector r;

  printf("The main (slow) loop starts:\n");
  printf("Some information:  input data is xdim=%3d ydim=%3d zdim=%3d:\n",xdim,ydim,zdim);
  printf("                  output data is xdim=%3d ydim=%3d zdim=%3d:\n",target_x,target_y,1);

  /* the total size of the system is  */
  Vector totalsize;
  totalsize.x = xdim*cellsize.x;
  totalsize.y = ydim*cellsize.y;

  /* the cellsize in the target resolution is different: */
  Vector targetcellsize;
  targetcellsize.x = totalsize.x / (double) target_x;
  targetcellsize.y = totalsize.y / (double) target_y;
  
  /* KL(m). Arguments for MFM-functions: (variable) positions */
  struct fun_args_x arg2;
  /* Arguments for MFM-functions: (fixed) cell size */
  const struct fun_args_dx arg1 = { 
     cellsize.x
    ,cellsize.y
    ,cellsize.z
    ,pow2(cellsize.x)
    ,pow2(cellsize.y)
    ,pow2(cellsize.z)
    ,pow4(cellsize.x)
    ,pow4(cellsize.y)
    ,pow4(cellsize.z) 
  };

  for (ty = 0; ty < target_y; ty++)
  {
    printf("%s[%s", blue, nocol);
    for (tx = 0; tx < target_x; tx++)
    {
  	  /* set the tip x and y positions from for loop */
  	  tip.x = (double)tx * targetcellsize.x; /* gives 'real' position */
  	  tip.y = (double)ty * targetcellsize.y; /* gives 'real' position */
  	  tip.z = tipposition; /* KL(m). Unnecessary code, I think. */
  	  printf("%so", red);
  	  fflush(stdout);
  
  	  /* KL(m) .x changed into x,y,z - just to make sure they all are zero-ed */
      /* central_point_field[tippos].x = 0.;
      central_point_field[tippos].y = 0.; */
      central_point_field[tippos].z = 0.;
  
  	  /* calculate the demagnetising field for each cell in 
  	   * the sample at this tip position */
  	  for (unsigned int iz = 0; iz < zdim; iz++)
  	  {
  	    r.z = (double)iz * cellsize.z;
        arg2.z   = r.z-tip.z;
        arg2.z2  = pow2 (arg2.z);
        arg2.z4  = pow4 (arg2.z);
        arg2.z6  = pow6 (arg2.z);
  	    for (unsigned int iy = 0; iy < ydim; iy++)
    		{
    		  r.y = (double)iy * cellsize.y;
          arg2.y   = r.y-tip.y;
          arg2.y2  = pow2(arg2.y);
          arg2.y4  = pow4(arg2.y);
          arg2.y6  = pow6(arg2.y);
    		  for (unsigned int ix = 0; ix < xdim; ix++)
   		    {
   		      r.x = (double)ix * cellsize.x;
            arg2.x = r.x-tip.x;   
            
   		      /* what is our linear position in the array? */
   		      /* added xdim in the last factor (fangohr 17/09/2003 18:51) */
   		      arraypos = ix + (xdim * iy) + (xdim * ydim * iz); 
   
   		      m.x = spins[arraypos].x;
   		      m.y = spins[arraypos].y;
   		      m.z = spins[arraypos].z;
   
   		      /* KL(m). Changes follow... */
            /* tip == tip position */
   		      /* r   == cell (spin) position, aka arg.x,y,z */
            double tmp;
            switch (equation_type) {
              case dipole: 
                tmp= dnzFactorDipol(arg1,arg2)*( m.x*dnXZzDipol(arg2) + 
                                                 m.y*dnYZzDipol(arg2) + 
                                                 m.z*dnZZzDipol(arg2) );
                break;
              case cubic:  
                tmp= dnzFactor(arg1,arg2)*( m.x*dnXZzCubic(arg1,arg2) + 
                                            m.y*dnYZzCubic(arg1,arg2) + 
                                            m.z*dnZZzCubic(arg1,arg2) );
                break;
              case noncubic: 
                tmp= dnzFactor(arg1,arg2)*( m.x*dnXZzNonCubic(arg1,arg2) + 
                                            m.y*dnYZzNonCubic(arg1,arg2) + 
                                            m.z*dnZZzNonCubic(arg1,arg2) );
                break;
            }
    			  central_point_field[tippos].z += tmp;
   		    }
    		}
	    }
  	  tippos++;
  	  printf("\b%s.", green);
  	  fflush(stdout);
	  }
      printf("%s] %s%3.1f%%%s\n", blue, yellow, ((double)ty / (double)target_y)*100 , nocol);
  }

  /* normalise? */

  printf("Normalising data... ");
  fflush(stdout);

  for (i = 0; i < (target_x*target_y); i++)
    {
      /*if (central_point_field[i].x > max_cd.x) max_cd.x = central_point_field[i].x;
      if (central_point_field[i].y > max_cd.y) max_cd.y = central_point_field[i].y;
      if (central_point_field[i].x < min_cd.x) min_cd.x = central_point_field[i].x;
      if (central_point_field[i].y < min_cd.y) min_cd.y = central_point_field[i].y;*/
      if (central_point_field[i].z > max_cd.z) max_cd.z = central_point_field[i].z;
      if (central_point_field[i].z < min_cd.z) min_cd.z = central_point_field[i].z;
    }
    
  for (i = 0; i < (target_x*target_y); i++)
    {
      /*if (!isnan(central_point_field[i].x)) 
	{
	  central_point_field[i].x = (central_point_field[i].x - min_cd.x) / max_cd.x;
	}
      else central_point_field[i].x = 0;
      if (!isnan(central_point_field[i].y))
	{
	  central_point_field[i].y = (central_point_field[i].y - min_cd.y) / max_cd.y;
	}
      else central_point_field[i].y = 0;*/
      if (!isnan(central_point_field[i].z))
	{
	  central_point_field[i].z = (central_point_field[i].z - min_cd.z) / max_cd.z;
	}
      else central_point_field[i].z = 0;
    }

  printf("%sdone%s\n", green, nocol);

  /* write data out to a file */

  printf("Attempting to open \"%s%s%s\" for write/append (raw data) ... ", blue, outputfile, nocol);
  fflush(stdout);

  outfile = fopen(outputfile, "a+");
  if (outfile == NULL)
    {
      printf("\n");
      lazy_error("Couldn't create/append output file... sorry :(");
    }
  else
    {
      for (i = 0; i < (target_x*target_y); i++)
	{
	  if ((i % target_y == 0) && (i != 0)) fprintf(outfile, "\n");
	  /*fprintf(outfile, "%f %f %f\n", central_point_field[i].x, central_point_field[i].y, central_point_field[i].z);*/
    /* KL(m) changed component into: z */
	  fprintf(outfile, "%f\n", central_point_field[i].z);
	}
      fclose(outfile);
    }

  printf("%sdone%s\n", green, nocol);

  /* write some VTK rectilinear ascii file */

  char *vtkoutputfile = strcat(outputfile, ".vtk");

  printf("Writing VTK file \"%s%s%s\"... ", blue, vtkoutputfile, nocol);
  fflush(stdout);

  FILE *vtkoutfile = fopen(vtkoutputfile, "w+");
  if (vtkoutfile == NULL)
    {
      printf("\n");
      lazy_error("Couldn't create VTK file... sorry :(");
    }
  else
    {
      /* game on ... */

      fprintf(vtkoutfile, "# vtk DataFile Version 2.0\n");
      fprintf(vtkoutfile, "Output from demagderiv-analytical.c\n");
      fprintf(vtkoutfile, "ASCII\n"); /* maybe do binary later... */
      fprintf(vtkoutfile, "DATASET RECTILINEAR_GRID\n");
      fprintf(vtkoutfile, "DIMENSIONS %d %d %d\n", target_x, target_y, 1); /* always flat */
      fprintf(vtkoutfile, "X_COORDINATES %d int\n", target_x);
      for (i=0; i<target_x; i++) fprintf(vtkoutfile, "%d ", i);
      fprintf(vtkoutfile, "\n");
      fprintf(vtkoutfile, "Y_COORDINATES %d int\n", target_y);
      for (i=0; i<target_y; i++) fprintf(vtkoutfile, "%d ", i);
      fprintf(vtkoutfile, "\n");
      fprintf(vtkoutfile, "Z_COORDINATES %d int\n", 1);
      for (i=0; i<1; i++) fprintf(vtkoutfile, "%d ", i);
      fprintf(vtkoutfile, "\n");
      fprintf(vtkoutfile, "POINT_DATA %d\n", target_x*target_y*1);
      fprintf(vtkoutfile, "SCALARS z-comp-demag float 1\nLOOKUP_TABLE default\n");
      /* KL(m) changed component into: z*/
      for (i=0; i<target_x*target_y; i++) fprintf(vtkoutfile, "%g\n", central_point_field[i].z);

      fclose(vtkoutfile);
    }
  
  printf("%sdone%s\n", green, nocol);
  
  /* everything went OK - good */

  printf("Pretty much completed; just freeing vector memory... ");
  fflush(stdout);

  /* free allocated memory */

  free(central_point_field);
  free(spins);
  free(binarydata);
  free(infiledata);
  printf("%sdone%s\n%sThanks for watching%s %s:)%s\n", green, nocol, yellow, nocol, blue, nocol);

  return 0;

}

