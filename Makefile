# demagderiv.c - Makefile
#
# This file is part of ovf2mfm.
# 
# Ovf2mfm is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Ovf2mfm is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with ovf2mfm (in the file license.txt).  
# If not, see <http://www.gnu.org/licenses/>.
#
# default target is to build the optimised executable for current machine

opt:
	gcc -ansi -std=c99 -Wall -mtune=native -O2 demagderiv.c -lm -o demagderiv

demo:
	time ./demagderiv < dropletzf350.omf data 100e-9 31 31


# documentation
doc-html:
	cd doc && rst2html.py usage.txt > usage.html

doc-pdf:
	cd doc && rst2latex.py usage.txt > usage.tex
	cd doc && pdflatex usage.tex
	cd doc && pdflatex usage.tex

doc:
	make doc-html
	make doc-pdf

all:
	make opt
	make demo
	make doc-html
	make doc-pdf

# other targets for portability
noopt:
	gcc -ansi -std=c99 -Wall demagderiv.c -lm -o demagderiv-noopt

# and for debugging
debug:
	gcc -g -std=c99 -Wall -O demagderiv.c -lm -o demagderiv-debug


